package com.orange.producerkafka.model;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class GenerateData {


    int numEmEI = 10;
    int numEvent = 3;

    Map<String, String> listData =  new HashMap<>();

    public GenerateData() {
        for (int i=1; i<=numEmEI; i++){
            int randomNum = ThreadLocalRandom.current().nextInt(1, numEvent + 1);
            listData.put(Integer.toString(i), Integer.toString(randomNum));
        }
    }

    @Bean
    public Map<String, String> getData(){
        return this.listData;
    }


    @Bean("getNumEMAI")
    public int getNumEmai(){
        return numEmEI;
    }



}
