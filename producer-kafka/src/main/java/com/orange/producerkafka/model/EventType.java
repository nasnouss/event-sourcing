package com.orange.producerkafka.model;

public enum EventType {


    // Initialize the elements from Constructor.
    // The element is always final, static
    COMMANDE_IPHONE("1", "Commande Iphone"), CHANGE_NUMBER("2", "Changement Numero"), SOUSCRIPTION_FORFAIT("3", "souscription forfait");

    private String code;
    private String text;

    // Constructor of Enum internal use only
    // Modifier of constructor is private
    // If you do not declare private, java alway understand is private.
    private EventType(String code, String text) {
        this.code = code;
        this.text = text;
    }


    // Static method return Gender by code.
    public static EventType getEventTypeByCode(String code) {
        for (EventType type : EventType.values()) {
            if (type.code.equals(code)) {
                return type;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}