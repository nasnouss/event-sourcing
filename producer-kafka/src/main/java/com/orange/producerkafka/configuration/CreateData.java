package com.orange.producerkafka.configuration;


import com.orange.producerkafka.kafka.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class CreateData {


    @Autowired
    Producer producer;


    @Scheduled(cron = "*/10 * * * * *")
    public void scheduleTaskUsingCronExpression() {

        producer.createMessage();
    }
}
