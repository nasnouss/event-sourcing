package com.orange.producerkafka.kafka;

import com.orange.producerkafka.model.Event;
import com.orange.producerkafka.model.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class Producer {

    private static final String TOPIC = "commandes";

    @Autowired
    KafkaTemplate<String, Event> kafkaTemplate;


    @Autowired
    Map<String, String> listData;


    @Autowired
    int numEmei;

    public void createMessage(){

        String randomNum = Integer.toString(ThreadLocalRandom.current().nextInt(1, numEmei + 1));
        kafkaTemplate.send(TOPIC, randomNum,  new Event(randomNum, EventType.getEventTypeByCode(listData.get(randomNum)).getText(), "bla bla bla", Long.toString(System.currentTimeMillis())));
    }



}
