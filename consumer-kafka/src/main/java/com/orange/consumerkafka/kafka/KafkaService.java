package com.orange.consumerkafka.kafka;


import com.datastax.driver.core.*;
import com.datastax.oss.protocol.internal.request.Prepare;
import com.orange.producerkafka.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class KafkaService {


    @Autowired
    Session session;

    @Autowired
    PreparedStatement preparedStatement;

    @KafkaListener(topics = "commandes", groupId = "PNS", containerFactory = "userKafkaListenerFactory")
    public void consumeJson(Event user) {

        BoundStatement boundStatement =  preparedStatement.bind();

        boundStatement.setString("imei", user.getEMEI());
        boundStatement.setString("timestamp", user.getTimestamp());
        boundStatement.setString("event_type", user.getEventType());
        boundStatement.setString("commentary", user.getDescription());

         session.executeAsync(boundStatement);


    }

}