package com.orange.consumerkafka.configuration;

import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.LoadBalancingPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class CassandraConfiguration {


    PreparedStatement preparedStatement;



    @Bean("session")
    public Session getSession() {

       Cluster cluster = getCluster();
       Session session = cluster.connect("pns");

        preparedStatement =  session.prepare("INSERT INTO pns.event_soucing (imei , timestamp , event_type , commentary ) VALUES ( ?, ?, ?, ?);");


    return session;

}


    @Bean
    public PreparedStatement getInsertStatement(){
        return preparedStatement;
    }



    @Bean
    public Cluster getCluster(){

        QueryOptions qo = new QueryOptions();
        qo.setDefaultIdempotence(true);

        PoolingOptions po = new PoolingOptions();

        po.setConnectionsPerHost(HostDistance.LOCAL, 2, 10000);
        po.setConnectionsPerHost(HostDistance.REMOTE, 2, 2000);


        Cluster.Builder builder = Cluster.builder()
            .addContactPoints("cassdev")
            .withQueryOptions(qo)
            .withCredentials("cassandra", "cassandra")
            .withPort(9042)
            .withoutJMXReporting()
            .withPoolingOptions(po);



        Cluster cluster = builder.build();

    return cluster;
    }


}
