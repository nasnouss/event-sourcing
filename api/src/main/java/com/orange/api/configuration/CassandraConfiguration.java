package com.orange.api.configuration;

import com.datastax.driver.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class CassandraConfiguration {


    PreparedStatement preparedStatement;



    @Bean("session")
    public Session getSession() {

        Cluster cluster = getCluster();
        Session session = cluster.connect("pns");

        preparedStatement =  session.prepare("SELECT * FROM event_soucing WHERE IMEI=:IMEI ;");


        return session;

    }


    @Bean
    public PreparedStatement getInsertStatement(){
        return preparedStatement;
    }



    @Bean
    public Cluster getCluster(){

        QueryOptions qo = new QueryOptions();
        qo.setDefaultIdempotence(true);

        PoolingOptions po = new PoolingOptions();

        po.setConnectionsPerHost(HostDistance.LOCAL, 2, 10000);
        po.setConnectionsPerHost(HostDistance.REMOTE, 2, 2000);


        Cluster.Builder builder = Cluster.builder()
                .addContactPoints("cassdev")
                .withQueryOptions(qo)
                .withCredentials("cassandra", "cassandra")
                .withPort(9042)
                .withoutJMXReporting()
                .withPoolingOptions(po);



        Cluster cluster = builder.build();

        return cluster;
    }
}
