package com.orange.api.DTO;

import com.datastax.driver.core.Row;

public class ListEventDTO {

    private String EMEI ;
    private String eventType;
    private String description;
    private String timestamp;

    public ListEventDTO() {
    }

    public ListEventDTO(String EMEI, String eventType, String description, String timestamp) {
        this.EMEI = EMEI;
        this.eventType = eventType;
        this.description = description;
        this.timestamp = timestamp;
    }

    public ListEventDTO(Row r) {
        this.EMEI = r.getString("imei");
        this.eventType = r.getString("event_type");
        this.description = r.getString("commentary");
        this.timestamp = r.getString("timestamp");

    }

    public String getEMEI() {
        return EMEI;
    }

    public void setEMEI(String EMEI) {
        this.EMEI = EMEI;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
