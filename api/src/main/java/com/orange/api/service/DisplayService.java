package com.orange.api.service;

import com.datastax.driver.core.*;
import com.google.common.base.Function;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.orange.api.DTO.ListEventDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
public class DisplayService {

    @Autowired
    Session session;


    @Autowired
    PreparedStatement preparedStatement;


    public List<ListEventDTO>  getAllEventForOneEmei(String IMEI) throws ExecutionException, InterruptedException {

        BoundStatement boundStatement = preparedStatement.bind();
        boundStatement.setString("IMEI",IMEI);

        ResultSetFuture resultSetFuture = session.executeAsync(boundStatement);

        ListenableFuture<List<ListEventDTO>> result = Futures.transform(resultSetFuture, new Function<ResultSet, List<ListEventDTO>>() {
            public List<ListEventDTO> apply(ResultSet rs) {

                List<ListEventDTO> listJob = new ArrayList<>();

                for (Row r : rs) {
                    listJob.add(new ListEventDTO(r));
                }

                return listJob;
            }
        });


        return result.get();
    }
    }

