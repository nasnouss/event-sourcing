package com.orange.api.controller;


import com.orange.api.DTO.ListEventDTO;
import com.orange.api.service.DisplayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@CrossOrigin
@RequestMapping("/event")
@Api(value = "job", description = "Events endpoint")
public class EventController {


    @Autowired
    DisplayService displayService;

    @CrossOrigin
    @ResponseBody
    @ApiOperation(value = " Display all event for an EMAI")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully formation created"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource")
    })
    @RequestMapping(path = "all", method = RequestMethod.GET, produces = "application/json")
    public List<ListEventDTO> displayOneJob(@RequestParam(name = "EMEI") String EMEI) throws ExecutionException, InterruptedException {

        List<ListEventDTO> result = displayService.getAllEventForOneEmei(EMEI);

        return result;
    }

}
